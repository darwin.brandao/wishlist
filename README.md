# WishList

WishList is a very simple script I wrote to keep track of things I wish to buy or do, but that are not important enough to be in a TO-DO list.

It creates a file called "wishes" in the same directory the script is in.

## Usage

When running './wish.sh' for the first time, it's going to create a symbolic link called 'wish' in ~/.local/bin/ , so you can run the script by typing 'wish' in any directory.

### Add new wish entry
```
$ wish Buy a coffe for Darwin
```

When you run 'wish' with arguments, it writes all the arguments to the 'wishes' file.

### List/edit wishes
```
$ wish
```

Running the 'wish' command, without any arguments, opens the 'wishes' file in your default text editor (the one in $EDITOR environment variable).

## Conclusion
It's a very simple script to keep track of wishes, ideas and things you'd like to buy or do. You can do whatever you want with it, it's MIT licensed.

## License

MIT License
