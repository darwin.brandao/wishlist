#!/bin/bash
# ===========================================
# License: MIT
# Made by: Darwin Brandão
# GitLab:  https://gitlab.com/darwin.brandao/
# ===========================================

# Directory of the script, not the working directory
DIR=$( cd -- "$( dirname -- "$( readlink -f "${BASH_SOURCE[0]}" )" )" &> /dev/null && pwd )

# If 'wish' command doesn't exist, create symbolic link in ~/.local/bin
[ $(command -v wish) ] || ln -s "$DIR/wish.sh" ~/.local/bin/wish

# 'wishes' file path
wishes="${DIR}/wishes"

# If there are no arguments, open 'wishes' file in the default text editor, otherwise echo arguments to 'wishes' file
[ $# -eq 0 ] && $EDITOR $wishes || echo "$@" >> $wishes
